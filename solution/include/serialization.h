

#ifndef SERIALIZATION_H
#define SERIALIZATION_H

#include <stdbool.h>
#include "bmp.h"
#include "image.h"
#include "find_error.h"

enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR
};

bool read_pic (char*, struct Image *);
bool create_pic(char*, struct Image *, struct Image *);

#endif
