

#ifndef IMAGE_H
#define IMAGE_H
#include <stdint.h>


struct Coordinates {
    uint64_t x;
    uint64_t y;
};

struct Pixel {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

struct Image {
    uint64_t width;
    uint64_t height;
    struct Pixel* data;
};



struct Image createImage(const uint64_t width, const uint64_t height);

#endif
