

#ifndef ROTATION_H
#define ROTATION_H
#include "image.h"

enum Angle {
    ROTATE_0 = 0,
    ROTATE_90 = 90,
    ROTATE_180 = 180,
    ROTATE_270 = 270
};

struct Image rotateImage(struct Image sourceImage, enum Angle angle);
enum Angle parseAngle(const char* value);

#endif
