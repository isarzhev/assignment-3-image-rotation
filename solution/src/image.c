#include "image.h"
#include <malloc.h>
#include <assert.h>

struct Image createImage(const uint64_t width, const uint64_t height){
    struct Pixel* data = (struct Pixel*)malloc(height * width * (sizeof(struct Pixel)));
    if(data){
        struct Image new_image = {.width = width, .height = height, .data = data };
        return new_image;
    }
    else{
        assert(0 && "Filed to allocate memory");
    }
}


