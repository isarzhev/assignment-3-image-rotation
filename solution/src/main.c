#include "errors_types.h"
#include "find_error.h"
#include "image.h"
#include "rotation.h"
#include "serialization.h"
#include <stdbool.h>
#include <stdlib.h>


int main(int argc, char* const argv []) {
    if(argc < 4) {
        fputs("please, write all arguments", stderr);
        exit(EXIT_FAILURE);
    }
    struct Image image = {0};
    bool status = read_pic(argv[1], &image);


    if (!status){
        exit(EXIT_FAILURE);
    }
    enum Angle angle = parseAngle(argv[3]);

    struct Image new_image = rotateImage(image, angle);
    bool new_status = create_pic(argv[2], &image, &new_image);

    if(!new_status) {
        exit(EXIT_FAILURE);
    }
    return 0;
}
