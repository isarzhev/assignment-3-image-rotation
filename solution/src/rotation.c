

#include "rotation.h"
#include <stdint.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

enum Angle parseAngle(const char* value){
    int32_t clampedAngle = (atoi(value) % 360);
    return (enum Angle)(clampedAngle >= 0 ? clampedAngle : clampedAngle + 360); 
}

static struct Coordinates translate(struct Image sourceImage, struct Coordinates source, enum Angle angle) {
    switch(angle){
        case ROTATE_0:
            return source;
        case ROTATE_90:
            return (struct Coordinates){ .x = source.y, .y = sourceImage.width - 1 - source.x };
        case ROTATE_180:
            return (struct Coordinates){ .x = sourceImage.width - 1 - source.x, .y = sourceImage.height - 1 - source.y };
        case ROTATE_270:
            return (struct Coordinates){ .x = sourceImage.height - 1 - source.y, .y = source.x };
        default:
            assert(0 && "Unreachable");
    }
}

static struct Image createRotatedImage(struct Image sourceImage, enum Angle angle) {
    switch(angle){
        case ROTATE_0:
        case ROTATE_180:
            return createImage(sourceImage.width, sourceImage.height);
        case ROTATE_90:
        case ROTATE_270:
            return createImage(sourceImage.height, sourceImage.width);
        default:
            assert(0 && "Unreachable");
    }
}

static uint64_t getPixelOffset(struct Image image, struct Coordinates coordinates) {
    return coordinates.y * image.width + coordinates.x;
}

struct Image rotateImage(struct Image sourceImage, enum Angle angle) {
    struct Image newImage = createRotatedImage(sourceImage, angle);

    for (uint64_t x = 0; x < sourceImage.width; x++) {
        for (uint64_t y = 0; y < sourceImage.height; y++) {
            struct Coordinates source = (struct Coordinates){ .x = x, .y = y };
            struct Coordinates destination = translate(sourceImage, source, angle);
            newImage.data[getPixelOffset(newImage, destination)] = sourceImage.data[getPixelOffset(sourceImage, source)];
        }
    }

    return newImage;
}
